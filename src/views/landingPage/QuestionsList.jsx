import React, { Component } from 'react';
import { Check2, X } from 'react-bootstrap-icons';

class QuestionsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userSelectedKey: null,
      userSelectedLabel: null,
      submitted: false
    }
  };

  resetState() {
    console.log("tetsing")
    this.setState({
      userSelectedKey: null,
      userSelectedLabel: null,
      submitted: false
    });
  }

  handleResult() {
    // function to handle user submitted response

    if (this.state.userSelectedKey != null) {
      // case: when user response is not empty
      this.setState({ submitted: true });
      this.props.handleResult(this.state.userSelectedKey);
    };
  }

  handleNext() {
    // function to handle next button functionality
    this.props.handleNext();
    this.setState({
      userSelectedKey: null,
      userSelectedLabel: null,
      submitted: false
    });
  }

  handleChange(key, label) {
    // function to handle option change
    if (!this.state.submitted) {
      this.setState({ userSelectedKey: key, userSelectedLabel: label });
    }
  }

  render() {
    const { data } = this.props;
    let { cQuestionIndex } = this.props;
    return (
      <div>
        <h5 className="card-title">What does the sign mean ?</h5>

        {/* Question sign/image container */}
        <div>
          <img src={data[cQuestionIndex].imagePath} alt="..." />
        </div>

        {/* options list */}
        <div className="card-deck" style={{ padding: 20 }}>
          {data[cQuestionIndex].options.map(item => {
            return (
              <div className="card" onClick={() => this.handleChange(item.key, item.label)}>
                {
                  this.state.submitted ?
                    <div className={data[cQuestionIndex].solutionKey == item.key ? "card-body bg-success text-white disabled" : "card-body bg-danger text-white disabled"}><p>{item.label}</p></div>
                    : <div className={this.state.userSelectedKey == item.key ? "card-body bg-primary text-white" : "card-body"}><p>{item.label}</p></div>
                }
              </div>
            )
          })}
        </div>

        <div>
          {
            this.state.submitted ?
              <React.Fragment>
                <div>
                  <div className="clearfix">
                    Submitted answer : {this.state.userSelectedLabel}
                    
                      {this.state.userSelectedKey == data[cQuestionIndex].solutionKey ?
                        <Check2 color="green" size={36} /> :
                        <X color="red" size={36} />
                      }
                  </div>
                  <div>Correct Answer : {data[cQuestionIndex].solutionLabel}</div>
                </div>

                <p style={{ paddingTop: 15 }}>Please click next button</p>
              </React.Fragment>

              : <p class="lead">Selected answer : {this.state.userSelectedLabel}</p>
          }


        </div>

        {/* Submit button */}
        {
          !this.state.submitted ?
            <button className="btn btn-primary" onClick={() => this.handleResult()}>Submit</button>
            : <button className="btn btn-primary disabled" >Submit</button>
        }

        {/* Next button */}
        {
          this.props.cQuestionIndex <= this.props.quizQuestionsCount - 1 ?
            this.state.submitted ?
              <button className="btn btn-primary" onClick={() => this.handleNext()}>Next</button> :
              <button className="btn btn-primary disabled">Next</button>
            :
            null
        }
      </div>
    );
  }
}

export default QuestionsList;

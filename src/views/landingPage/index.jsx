import React, { Component } from 'react';
import data from '../../temp/data';
import QuestionsList from './QuestionsList';

class LandingPage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      cQuestionIndex: null,
      showQuiz: false,
      showScore: false,
      quizQuestions: [],
      quizQuestionsCount: 5,
      correctAnswers: []
    };

    this.questionListElement = React.createRef();

    this.handleStart = this.handleStart.bind(this);
    this.shuffleArray = this.shuffleArray.bind(this);
    this.handlePreprocessing = this.handlePreprocessing.bind(this);
    this.handleResult = this.handleResult.bind(this);
    this.handleNext = this.handleNext.bind(this);

  };

  shuffleArray(array) {
    // function to shuffle array elements in-place
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
  }


  handlePreprocessing() {
    // function to load quiz questions and options from hardcoded data

    let allQuestionsCount = data.signs.length;
    let n = this.state.quizQuestionsCount;

    // generate random question sets from all questions
    let nums = new Set();
    while (nums.size !== n) {
      nums.add(Math.floor(Math.random() * allQuestionsCount));
    }

    let qIndices = [...nums]; //convert set to an array
    let questions = [];
    for (let qIndex of qIndices) {
      // generating options from all labels
      let options = [];

      let o_set = new Set();
      o_set.add(qIndex)
      while (o_set.size !== 4) { o_set.add(Math.floor(Math.random() * allQuestionsCount)); };

      let o_arr = [...o_set]; //convert set to an array
      this.shuffleArray(o_arr); //shuffle options

      for (let o_item of o_arr) {
        options.push({
          key: o_item,
          label: data.signs[o_item].imageLabel
        });
      };

      // generating individual questions
      let question = {
        imagePath: data.signs[qIndex].imagePath,
        options: options,
        solutionLabel: data.signs[qIndex].imageLabel,
        solutionKey: qIndex
      };
      
      // appending all questions to an array
      questions.push(question);
    };

    // setting questions into state
    this.setState({
      quizQuestions: questions,
      cQuestionIndex: 0,
      correctAnswers: [],
      showQuiz: true,
      showScore: true
    })
  }

  handleStart() {
    // utility function to start or reset quiz
    this.handlePreprocessing();

    if(this.state.showQuiz){
      // reset state of questionlist component, if its already mounted
      this.questionListElement.current.resetState();
    }
    
  };



  handleResult(answer) {
    // function to handle user responses
    let { correctAnswers, cQuestionIndex, quizQuestions } = this.state;

    if (answer == quizQuestions[cQuestionIndex].solutionKey) {
      // case: user has responded correctly
      correctAnswers.push(cQuestionIndex);
      this.setState({ correctAnswers });
    }
  };

  handleNext() {
    // function to handle next button functionality
    if (this.state.cQuestionIndex == this.state.quizQuestionsCount - 1) {
      // case: All questions answered
      this.setState({ showQuiz: false });
    } else {
      // case: some questions still not answered
      this.setState({ cQuestionIndex: this.state.cQuestionIndex + 1 });
    }
  }

  render() {
    return (
      <div>
        <div className="jumbotron">
          <h1 className="display-4">Traffic Signs Quiz</h1>
          <div>
            <button className="btn btn-primary" onClick={() => this.handleStart()}>Start/Reset</button>
          </div>
          
          {/* Element to show user Score */}
          {
            this.state.showScore ?
              <div className="text-center">
                <h2>Total Score : {this.state.correctAnswers.length}</h2>
              </div>
              : null
          }

          {/* Element to show quiz questions */}
          {
            this.state.showQuiz ?
              <React.Fragment>
                <div className="card text-center">
                  
                  {/* card header to show track of questions answered */}
                  <div class="card-header">  
                    <h5 >Question {this.state.cQuestionIndex + 1} / {this.state.quizQuestionsCount} </h5>
                  </div>

                  {/* Display questions */}
                  <div className="card-body">
                    <QuestionsList
                      ref = {this.questionListElement}
                      data={this.state.quizQuestions}
                      cQuestionIndex={this.state.cQuestionIndex}
                      quizQuestionsCount={this.state.quizQuestionsCount}
                      handleResult={(ans) => this.handleResult(ans)}
                      handleNext={() => this.handleNext()}
                    />
                  </div>
                </div>
              </React.Fragment>
              : null
          }
        </div>
      </div>
    );
  }
}

export default LandingPage;

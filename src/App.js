import React, { Component } from 'react';
import LandingPage from './views/landingPage';


class App extends Component {
  render() {
    return (
      <div class="container">
        <LandingPage />
      </div>
    );
  }
}

export default App;

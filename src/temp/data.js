// Traffic signal images and its labels are hardcoded here
// source  : https://en.wikipedia.org/wiki/Road_signs_in_India
// images are stored in public folder

module.exports = {
  signs: [
    {
      imagePath: "images/axle-weight-limit.jpg",
      imageLabel: "Axle weight limit"
    },
    {
      imagePath: "images/busstop.jpg",
      imageLabel: "Bus Stop"
    },
    {
      imagePath: "images/cycle-track.jpg",
      imageLabel: "Cycle track"
    },
    {
      imagePath: "images/filling-station.jpg",
      imageLabel: "Filling Station"
    },
    {
      imagePath: "images/first-aid.jpg",
      imageLabel: "First aid post"
    },
    {
      imagePath: "images/giveway.jpg",
      imageLabel: "Give way"
    },
    {
      imagePath: "images/height-limit.jpg",
      imageLabel: "Height limit"
    },
    {
      imagePath: "images/horn-prohibited.jpg",
      imageLabel: "Horn prohibited"
    },
    {
      imagePath: "images/hotel.jpg",
      imageLabel: "Hotel"
    },
    {
      imagePath: "images/keep-left.jpg",
      imageLabel: "Keep left"
    },
    {
      imagePath: "images/length-limit.jpg",
      imageLabel: "Length limit"
    },
    {
      imagePath: "images/max-speed-70.jpg",
      imageLabel: "Maximum speed limit (70 km/h)"
    },
    {
      imagePath: "images/no-entry-goods-vehicles.jpg",
      imageLabel: "No entry for goods vehicles"
    },
    {
      imagePath: "images/no-entry-motor-vehicles.jpg",
      imageLabel: "No entry for motor vehicles"
    },
    {
      imagePath: "images/no-entry-pedestrian.jpg",
      imageLabel: "No entry for pedestrians"
    },
    {
      imagePath: "images/no-entry.jpg",
      imageLabel: "No entry"
    },
    {
      imagePath: "images/no-left-turn.jpg",
      imageLabel: "No left turn"
    },
    {
      imagePath: "images/no-overtaking.jpg",
      imageLabel: "No overtaking"
    },
    {
      imagePath: "images/no-parking.jpg",
      imageLabel: "No parking"
    },
    {
      imagePath: "images/no-right-turn.jpg",
      imageLabel: "No right turn"
    },
    {
      imagePath: "images/no-stopping.jpg",
      imageLabel: "No stopping"
    },
    {
      imagePath: "images/no-vehicles-both-direction.jpg",
      imageLabel: "No vehicles in both directions"
    },
    {
      imagePath: "images/one-way-traffic.jpg",
      imageLabel: "One-way traffic"
    },
    {
      imagePath: "images/parking.jpg",
      imageLabel: "Parking"
    },
    {
      imagePath: "images/refreshments.jpg",
      imageLabel: "Refreshments"
    },
    {
      imagePath: "images/restaurents.jpg",
      imageLabel: "Restaurant"
    },
    {
      imagePath: "images/stop.jpg",
      imageLabel: "Stop"
    },
    {
      imagePath: "images/turn-left-ahead.jpg",
      imageLabel: "Turn left ahead"
    },
    {
      imagePath: "images/turn-right-ahead.jpg",
      imageLabel: "Turn right ahead"
    },
    {
      imagePath: "images/weight-limit.jpg",
      imageLabel: "Weight limit"
    },
  ]
}